<?php
//Get params from url and do sql lookup

include 'Base.php';
header('Content-type: application/json');
session_start();

$link = mysqli_connect($hostname, $username, $password, $dbName);

// Check connection
if ($link === false) {
    die("ERROR: Could not connect. " . mysqli_connect_error());
}

$Return = null;
if (isset($_GET['Return'])) {
    $Return = $_GET['Return'];
    unset($_GET['Return']);
}

echo $Return;

$CourseworkID = null;
if (isset($_GET['ID'])) {
    $CourseworkID = $_GET['ID'];
}

$Field = null;
if (isset($_GET['Field'])) {
    $Field = $_GET['Field'];
} else if (isset($_GET['username'])) {
    $Field = 'Username';
}

$LoggedInUserId = null;
$UserType       = null;
$TYPE_STUDENT   = 'Student';
$TYPE_STAFF     = 'Staff';
if (isset($_SESSION['LoggedInUserId'])) {
    $LoggedInUserId = $_SESSION['LoggedInUserId'];

    $sql = "SELECT (SELECT count(*) FROM staff WHERE staff.ParentID = " . $LoggedInUserId . ") , (SELECT count(*) FROM student WHERE student.ParentID = " . $LoggedInUserId . ") from user WHERE ID = " . $LoggedInUserId;
    if ($result = mysqli_query($link, $sql)) {
        if (mysqli_num_rows($result) > 0) {
            while ($row = $result->fetch_row()) {

                //echo "staff: " . $row[0];
                //echo "student: " . $row[1];

                if ($row[0] == 1) {
                    $UserType = $TYPE_STAFF;
                } else if ($row[1] == 1) {
                    $UserType = $TYPE_STUDENT;
                }

                //echo $UserType;
            }
        }
        mysqli_free_result($result);
    }

}

switch ($Field) {
    case 'Username':
        $pass  = passwordEncryption(urldecode($_GET['password']));
        $email = urldecode($_GET['username']);

        $loginsql = "SELECT ID,(SELECT count(*) FROM staff WHERE staff.ParentID = ID) , (SELECT count(*) FROM student WHERE student.ParentID = ID) FROM user WHERE binary Email = '" . $email . "'  AND binary Password = '" . $pass . "'";
        //echo $loginsql;
        if ($result = mysqli_query($link, $loginsql)) {
            if (mysqli_num_rows($result) > 0) {

                while ($row = $result->fetch_row()) {
                    $response_array['ID']       = $row[0];
                    $_SESSION['LoggedInUserId'] = $row[0];
                    if ($row[1] == 1) {
                        $UserType = $TYPE_STAFF;
                    } else if ($row[2] == 1) {
                        $UserType = $TYPE_STUDENT;
                    }

                    echo json_encode($response_array);

                }

                mysqli_free_result($result);

            } else {
                $response_array['status'] = 'fail';
                $_SESSION['status']       = 'fail';
                unset($_SESSION['LoggedInUserId']);
                echo json_encode($response_array);
            }
            if ($Return != null) {
            } else {
                if ($UserType == $TYPE_STAFF) {

                    header('Location: ../UI/Frontend/tasks.php');
                } else {
                    header('Location: ../UI/Frontend/tasks.php');
                }
                //
            }

        }

    case 'Feedback':
        $FeedbackId = null;
        if (isset($_GET['FeedbackID'])) {
            $FeedbackId = $_GET['FeedbackID'];
        }
        $sql = "SELECT Mark, Feedback from feedback WHERE ID = " . $FeedbackId . "";
        break;
    //echo $sql;
    case 'Spec':
        $specId = null;
        if (isset($_GET['SpecID'])) {
            $specId = $_GET['SpecID'];
        }
        $sql = "SELECT Title, Description from coursework WHERE ID = " . $specId . "";
        break;
    case 'Coursework':

        //echo $ModuleId;

        $sql = "SELECT ID, Title, Deliverables,
                            CASE WHEN Review IS NULL THEN 0 ELSE Review END  AS 'CourseworkReady', ReleaseDate  from coursework WHERE 1=1 ";
        $ModuleCode = null;
        if (isset($_GET['ModuleCode'])) {
            $ModuleCode = $_GET['ModuleCode'];
            //$sql .= " AND Module=(SELECT ID from module where ModuleCode=" . $ModuleCode . ") ";
            $sql .= " AND find_in_set(ID, (SELECT Courseworks from module where ModuleCode=" . $ModuleCode . "))";
        }
        //echo $sql;
        break;
    case 'Module':
        $sql = "SELECT ID, Name, ModuleCode, Courseworks ";
        if ($LoggedInUserId != null) {
            if ($UserType == $TYPE_STUDENT) {
                $sql .= " , (SELECT CONCAT('TRUE')) AS 'Student' from module WHERE find_in_set(module.ID,(SELECT GROUP_CONCAT(ModuleID) from module_student WHERE StudentID = " . $LoggedInUserId . ")) ";

                //$sql .= " WHERE WHERE find_in_set(ID,(SELECT GROUP_CONCAT(ModuleID)) FROM module_student WHERE StudentID = " . $LoggedInUserId . ")";
            } else {
                $sql .= ",

                        (
                        SELECT
                            COUNT(coursework.ID)
                        FROM
                            coursework
                        WHERE
                            FIND_IN_SET(
                                coursework.ID,
                                (
                                SELECT
                                    GROUP_CONCAT(Courseworks)
                                FROM
                                    module
                                WHERE
                                    FIND_IN_SET(
                                        module.ID,
                                        (
                                        SELECT
                                            GROUP_CONCAT(
                                                CONCAT(Modules, ',', ModulesManaged)
                                            )
                                        FROM
                                            staff
                                        WHERE
                                            ParentID = " . $LoggedInUserId . "
                                    )
                                    )
                            )
                            )
                    ) AS 'AllCourseworks',
                    (
                    SELECT
                        COUNT(DISTINCT coursework.ID)
                    FROM
                        coursework
                    WHERE
                        FIND_IN_SET(
                            coursework.ID,
                            (
                            SELECT
                                GROUP_CONCAT(Courseworks)
                            FROM
                                module
                            WHERE
                                FIND_IN_SET(
                                    module.ID,
                                    (
                                    SELECT
                                        GROUP_CONCAT(
                                            CONCAT(Modules, ',', ModulesManaged)
                                        )
                                    FROM
                                        staff
                                    WHERE
                                        ParentID = " . $LoggedInUserId . "
                                )
                                )
                        )
                        ) AND coursework.Review = 1
                    ) AS 'ReadyCourseworks',

                 (SELECT ModulesManaged FROM staff WHERE ParentID = " . $LoggedInUserId . ") AS 'ModulesManaged', (SELECT Reviewing FROM staff WHERE ParentID = " . $LoggedInUserId . ") AS 'Reviewing'  from module WHERE find_in_set(ID,(SELECT GROUP_CONCAT( CONCAT(Modules, ',', ModulesManaged, ',', Reviewing)) FROM staff WHERE ParentID = " . $LoggedInUserId . "))";
            }
            //echo $sql;
        }

        break;

    case 'Tasks':
        //$sql = "SELECT ID, State, Description, ExpiryDate, Message,  (SELECT Name from module where ID = (SELECT Module from coursework where find_in_set(task.RelatedDeliverable, coursework.Deliverables)))  AS 'Coursework' from task where Dismissed = 0";

        $sql = "SELECT ID, State, Description, ExpiryDate, Message,  (SELECT Title from Coursework where find_in_set(RelatedDeliverable, coursework.Deliverables)) AS 'CourseworkTitle', (SELECT Name from module where find_in_set(task.RelatedCoursework, module.Courseworks))  AS 'ModuleTitle' from task where Dismissed = 0";
        if ($LoggedInUserId != null) {
            if ($UserType == $TYPE_STUDENT) {
                $sql .= " AND find_in_set(" . $LoggedInUserId . ",RelatedUsers) ";

                //$sql .= " WHERE WHERE find_in_set(ID,(SELECT GROUP_CONCAT(ModuleID)) FROM module_student WHERE StudentID = " . $LoggedInUserId . ")";
            } else {
                $sql .= " AND find_in_set(" . $LoggedInUserId . ",RelatedUsers) ";
            }

        }
        //echo $sql;
        break;

    case 'Date':
        $sql = "SELECT ReleaseDate, ID AS 'Coursework', Title AS 'CourseworkTitle',
                (SELECT MAX(SubmissionDate) from deliverable where find_in_set(deliverable.ID, Deliverables) ) AS 'SubmissionDate',
                (SELECT MAX(FeedbackDate) from deliverable where find_in_set(deliverable.ID, Deliverables) ) AS 'FeedbackDate'

                from coursework
                where ";
        if ($LoggedInUserId != null) {
            if ($UserType == $TYPE_STUDENT) {
                //$sql .= " find_in_set(Module,(SELECT GROUP_CONCAT(ModuleID) from module_student WHERE StudentID = '" . $LoggedInUserId . "'))";
                $sql .= "
                            FIND_IN_SET(
                                coursework.ID,
                                (
                                SELECT
                                    GROUP_CONCAT(Courseworks)
                                FROM
                                    module
                                WHERE find_in_set(module.ID,(SELECT GROUP_CONCAT(ModuleID) from module_student WHERE StudentID = " . $LoggedInUserId . "))
                            )
                            )";

            } else {
                $sql .= "
                            FIND_IN_SET(
                                coursework.ID,
                                (
                                SELECT
                                    GROUP_CONCAT(Courseworks)
                                FROM
                                    module
                                WHERE
                                    FIND_IN_SET(
                                        module.ID,
                                        (
                                        SELECT
                                            GROUP_CONCAT(
                                                CONCAT(Modules, ',', ModulesManaged)
                                            )
                                        FROM
                                            staff
                                        WHERE
                                            ParentID = " . $LoggedInUserId . "
                                    )
                                )
                            )
                        ) ";
            }
            //echo $sql;
        }

        break;

    case 'Users':
        $sql = "SELECT ID, OtherNames, Email from user";
        break;

    case 'ReleaseDate':
        $Query = "ReleaseDate";
        break;

    case 'FeedbackDate':
        $Query = "FeedbackDate";
        break;

    case 'Review':

        $sql          = "SELECT (SELECT Title from coursework WHERE coursework.ID = review.Coursework )AS 'Title', Passed from review ";
        $CourseworkID = null;
        if (isset($_GET['CourseworkID'])) {
            $CourseworkID = $_GET['CourseworkID'];
            $sql .= " WHERE Coursework='" . $CourseworkID . "'";
        }

        //echo $sql;
        break;

    case 'ReviewForm':
        $CourseworkID = null;
        if (isset($_GET['CourseworkID'])) {
            $CourseworkID = $_GET['CourseworkID'];

            $sql = "SELECT (SELECT Title from coursework WHERE coursework.ID = review.Coursework )AS 'Title', (SELECT ModuleCode from module where find_in_set('" . $CourseworkID . "', Courseworks) ) AS 'AssesmentCode', (SELECT OtherNames from user where ID =(SELECT ParentID from staff where find_in_set((SELECT ID from Module where find_in_set('" . $CourseworkID . "', Courseworks )),Reviewing)) ) AS 'Reviewer', WeightingSizeLengthMatch, LearningOutcomesFulfilled, Clear, SuitableTimescale, AppropriateLevel, AppropriateMarkScheme, ConsistentMarkScheme, AcademicMisconductLimited, SecondMarking, EthicalApproval, RiskAssesment, SameReassessment,  Passed from review ";
            $sql .= " WHERE Coursework='" . $CourseworkID . "'";
        }

        //echo $sql;
        break;

    case 'Deliverable':
        $DeliverableId = null;
        if (isset($_GET['DeliverableId'])) {
            $DeliverableId = $_GET['DeliverableId'];
        }
        $sql = "SELECT (SELECT Description from type where type.ID = deliverable.Type) AS 'Description', SubmissionDate, (SELECT GROUP_CONCAT(Description) from type) AS 'Types' from deliverable where ID=" . $DeliverableId;
        //echo $sql;
        break;

    case 'Deliverables':
        /*
        $Query = "(SELECT GROUP_CONCAT(Description) from type WHERE find_in_set(type.ID,
        (SELECT GROUP_CONCAT(Type) FROM deliverable WHERE find_in_set(deliverable.ID,(SELECT Deliverables) ) ) )
        ) AS 'Deliverables'";
         */
        $Deliverables = null;
        if (isset($_GET['Deliverables'])) {
            $Deliverables = $_GET['Deliverables'];
        }
        $sql = "SELECT (SELECT Description FROM type WHERE type.ID = deliverable.Type) AS 'Name', ID, SubmissionDate, (SELECT Description FROM type WHERE type.ID = deliverable.Type) AS 'Type' from deliverable WHERE deliverable.ID IN (" . $Deliverables . ")";
        break;

    default:
        $Query = "ID, (SELECT Name from module WHERE Module = module.ID) AS 'Module', ReleaseDate, FeedbackDate, (SELECT OtherNames FROM user WHERE ID = (SELECT Reviewer FROM module WHERE module.ID = coursework.Module)) AS 'Reviewer', (SELECT GROUP_CONCAT(Description) from type WHERE find_in_set(type.ID,(SELECT GROUP_CONCAT(Type) FROM deliverable WHERE find_in_set(deliverable.ID,(SELECT Deliverables) ) ) ) ) AS 'Deliverables'";
        $sql   = "SELECT " . $Query . " from coursework WHERE ID=" . $CourseworkID;

}

// Attempt select query execution

if ($Field != 'Username') {
    if ($result = mysqli_query($link, $sql)) {
        if (mysqli_num_rows($result) > 0) {
            while ($row = $result->fetch_array(MYSQL_ASSOC)) {
                $myArray[] = $row;
            }
            echo json_encode($myArray);

            mysqli_free_result($result);

        }
    }
}
