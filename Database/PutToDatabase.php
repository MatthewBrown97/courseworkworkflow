<?php

include 'Base.php';

$link = mysqli_connect($hostname, $username, $password, $dbName);

// Check connection
if ($link === false) {
    die("ERROR: Could not connect. " . mysqli_connect_error());
}

$Form = null;
if (isset($_GET['Form'])) {
    $Form = $_GET['Form'];
}

switch ($Form) {
    case 'CourseworkElement':
        $CourseworkID = null;
        if (isset($_GET['CourseworkID'])) {
            $CourseworkID = $_GET['CourseworkID'];
        }

        $Module = null;
        if (isset($_GET['Module'])) {
            $Module = $_GET['Module'];
        }

        $ReleaseDate = null;
        if (isset($_GET['ReleaseDate'])) {
            $ReleaseDate = $_GET['ReleaseDate'];
        }

        $FeedbackDate = null;
        if (isset($_GET['FeedbackDate'])) {
            $FeedbackDate = $_GET['FeedbackDate'];
        }

        $Review = null;
        if (isset($_GET['Review'])) {
            $Review = $_GET['Review'];
        }

        $Deliverables = null;
        if (isset($_GET['Deliverables'])) {
            $Deliverables = $_GET['Deliverables'];
        }

        if ($CourseworkID == null) {
            $sql = "INSERT INTO coursework (Module, ReleaseDate, FeedbackDate, Review, Deliverables)
        		VALUES ('" . $Module . "', '" . $ReleaseDate . "', '" . $FeedbackDate . "', '" . $Review . "', " . $Deliverables . ")";
        } else {
            $sql = "UPDATE coursework (CourseworkID, Module, ReleaseDate, FeedbackDate, Review, Deliverables)
        		VALUES ('" . $CourseworkID . "', '" . $Module . "', '" . $ReleaseDate . "', '" . $FeedbackDate . "', '" . $Review . "', " . $Deliverables . ")";
        }
        break;

    default:
        $sql = "";
        break;
}

if ($sql != "") {
    //echo $sql;
    if (mysqli_query($link, $sql)) {
        $json_ret['success'] = true;
        echo json_encode($json_ret);
    } else {
        die(mysqli_error($link));
    }

}
