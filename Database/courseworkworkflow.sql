-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 04, 2018 at 01:02 PM
-- Server version: 5.7.17
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `courseworkworkflow`
--

-- --------------------------------------------------------

--
-- Table structure for table `coursework`
--

CREATE TABLE `coursework` (
  `ID` int(11) NOT NULL,
  `ReleaseDate` datetime(6) NOT NULL,
  `Review` int(11) DEFAULT NULL,
  `Deliverables` text NOT NULL,
  `Feedbacks` text NOT NULL,
  `Description` text NOT NULL,
  `Title` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coursework`
--

INSERT INTO `coursework` (`ID`, `ReleaseDate`, `Review`, `Deliverables`, `Feedbacks`, `Description`, `Title`) VALUES
(2, '2018-04-01 00:00:00.000000', NULL, '', '', '', 'Networking & UI ACW'),
(7, '2018-04-01 00:00:00.000000', NULL, '', '', '', 'Written exam'),
(8, '2018-02-15 00:00:00.000000', 1, '2,6,7', '', '', 'Group software application lifecycle management project '),
(9, '2018-04-01 00:00:00.000000', NULL, '', '', '', 'Examination'),
(10, '2018-04-01 00:00:00.000000', NULL, '', '', '', 'Online Test'),
(11, '2018-04-01 00:00:00.000000', NULL, '', '', '', 'Data Structure Analysis'),
(12, '2018-04-01 00:00:00.000000', NULL, '', '', '', 'Exam Quiz'),
(13, '2018-04-01 00:00:00.000000', NULL, '', '', '', 'Implementing a Simple Agent Chatbot to Suggest Christmas Presents'),
(14, '2018-02-01 00:00:00.000000', NULL, '8,9', '', '', 'ProcessFlow'),
(15, '2018-04-01 00:00:00.000000', NULL, '', '', '', 'Team Assignement'),
(16, '2018-04-01 00:00:00.000000', NULL, '', '', '', 'Final Exam');

-- --------------------------------------------------------

--
-- Table structure for table `deliverable`
--

CREATE TABLE `deliverable` (
  `ID` int(11) NOT NULL,
  `Type` int(11) NOT NULL,
  `SubmissionDate` datetime NOT NULL,
  `Submissions` text NOT NULL,
  `FeedbackDate` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deliverable`
--

INSERT INTO `deliverable` (`ID`, `Type`, `SubmissionDate`, `Submissions`, `FeedbackDate`) VALUES
(2, 2, '2018-05-04 14:00:00', '', '2018-05-05 00:00:00'),
(6, 6, '2018-05-04 14:00:00', '', '2018-05-03 00:00:00'),
(7, 7, '2018-05-04 14:00:00', '', '2018-05-02 00:00:00'),
(8, 2, '2018-04-25 14:00:00', '', '2018-05-05 00:00:00'),
(9, 4, '2018-04-25 14:00:00', '', '2018-05-05 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `ID` int(11) NOT NULL,
  `Mark` int(11) NOT NULL,
  `Feedback` text NOT NULL,
  `Student` int(11) NOT NULL,
  `Completed` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE `module` (
  `ID` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Courseworks` text NOT NULL,
  `Reviewer` int(11) NOT NULL,
  `ModuleCode` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`ID`, `Name`, `Courseworks`, `Reviewer`, `ModuleCode`) VALUES
(1, 'Networking and User Interface Design', '2,7', 3, '08140'),
(2, 'Agile Software Development', '8,9', 3, '08142'),
(3, 'Advanced Programming', '10,11', 3, '08143'),
(4, 'Artificial Intelligence', '12,13', 3, '08146'),
(5, 'Software Engineering', '14', 3, '08148'),
(6, 'Systems Analysis, Design and Process', '15,16', 3, '08144');

-- --------------------------------------------------------

--
-- Table structure for table `module_student`
--

CREATE TABLE `module_student` (
  `ID` int(11) NOT NULL,
  `ModuleID` int(11) NOT NULL,
  `StudentID` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `module_student`
--

INSERT INTO `module_student` (`ID`, `ModuleID`, `StudentID`) VALUES
(1, 1, 2),
(2, 1, 4),
(3, 3, 4),
(4, 2, 4),
(5, 4, 4),
(6, 5, 4),
(7, 6, 4);

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `ID` int(11) NOT NULL,
  `Coursework` int(11) NOT NULL,
  `ReviewDeadline` datetime NOT NULL,
  `Passed` tinyint(1) DEFAULT NULL,
  `WeightingSizeLengthMatch` tinyint(1) DEFAULT NULL,
  `LearningOutcomesFulfilled` tinyint(1) DEFAULT NULL,
  `Clear` tinyint(1) DEFAULT NULL,
  `SuitableTimescale` tinyint(1) DEFAULT NULL,
  `AppropriateLevel` tinyint(1) DEFAULT NULL,
  `AppropriateMarkScheme` tinyint(1) DEFAULT NULL,
  `ConsistentMarkScheme` tinyint(1) DEFAULT NULL,
  `AcademicMisconductLimited` tinyint(1) DEFAULT NULL,
  `SecondMarking` tinyint(1) DEFAULT NULL,
  `EthicalApproval` tinyint(1) DEFAULT NULL,
  `RiskAssesment` tinyint(1) DEFAULT NULL,
  `SameReassessment` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`ID`, `Coursework`, `ReviewDeadline`, `Passed`, `WeightingSizeLengthMatch`, `LearningOutcomesFulfilled`, `Clear`, `SuitableTimescale`, `AppropriateLevel`, `AppropriateMarkScheme`, `ConsistentMarkScheme`, `AcademicMisconductLimited`, `SecondMarking`, `EthicalApproval`, `RiskAssesment`, `SameReassessment`) VALUES
(1, 7, '2018-04-01 00:00:00', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 2, '2018-04-01 00:00:00', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 8, '2018-04-01 00:00:00', 0, 1, 1, 1, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL),
(4, 9, '2018-04-01 00:00:00', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `ParentID` int(11) NOT NULL,
  `Modules` text NOT NULL,
  `Reviewing` text NOT NULL,
  `ModulesManaged` text NOT NULL,
  `CourseworksAuthored` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`ParentID`, `Modules`, `Reviewing`, `ModulesManaged`, `CourseworksAuthored`) VALUES
(1, '', '2', '1', ''),
(3, '', '1', '', ''),
(5, '', '1,3,4,5', '2,6', '');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `ParentID` int(11) NOT NULL,
  `StudentNumber` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`ParentID`, `StudentNumber`) VALUES
(2, 528433),
(4, 528432);

-- --------------------------------------------------------

--
-- Table structure for table `submission`
--

CREATE TABLE `submission` (
  `ID` int(11) NOT NULL,
  `Submitted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE `task` (
  `ID` int(11) NOT NULL,
  `State` text NOT NULL,
  `Description` text NOT NULL,
  `RelatedUsers` text NOT NULL,
  `ExpiryDate` datetime NOT NULL,
  `RelatedDeliverable` text,
  `Type` int(11) NOT NULL,
  `RelatedCoursework` text,
  `Message` text,
  `Dismissed` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `task`
--

INSERT INTO `task` (`ID`, `State`, `Description`, `RelatedUsers`, `ExpiryDate`, `RelatedDeliverable`, `Type`, `RelatedCoursework`, `Message`, `Dismissed`) VALUES
(39, '2', 'Group software application lifecycle management project ', '4', '2018-05-02 00:00:00', NULL, 4, '8', 'This coursework has been released today', 1),
(36, '2', 'Networking & UI ACW Review Failed', '3', '2018-04-01 00:00:00', NULL, 3, '2', '', 0),
(31, '1', 'Program', '4', '2018-05-04 14:00:00', '2', 1, NULL, '', 0),
(32, '1', 'Portfolio', '4', '2018-05-04 14:00:00', '6', 1, NULL, '', 0),
(33, '1', 'Video Presentation', '4', '2018-05-04 14:00:00', '7', 1, NULL, '', 0),
(40, '1', 'Program', '5', '2018-05-05 00:00:00', '2', 2, NULL, NULL, 0),
(41, '1', '', '4', '2018-05-04 14:00:00', '8', 1, NULL, NULL, 0),
(42, '1', '', '4', '2018-05-04 14:00:00', '9', 1, NULL, NULL, 0),
(43, '1', '', '', '2018-05-05 00:00:00', '8', 2, NULL, NULL, 0),
(44, '1', '', '', '2018-05-05 00:00:00', '9', 2, NULL, NULL, 0),
(45, '2', 'Group software application lifecycle management project  Review Failed', '3', '2018-04-01 00:00:00', NULL, 3, '8', NULL, 0),
(46, '2', 'Examination Review Failed', '3', '2018-04-01 00:00:00', NULL, 3, '9', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `task_types`
--

CREATE TABLE `task_types` (
  `ID` int(11) NOT NULL,
  `Description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `task_types`
--

INSERT INTO `task_types` (`ID`, `Description`) VALUES
(1, 'DeliverableSubmissionDue'),
(2, 'FeedbackSubmissionDue'),
(3, 'ReviewFailed'),
(4, 'CourseworkReleased'),
(5, 'CourseworkCreated');

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE `type` (
  `ID` int(11) NOT NULL,
  `Description` text NOT NULL,
  `UserSubmitted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`ID`, `Description`, `UserSubmitted`) VALUES
(1, 'Presentation', 0),
(2, 'Program', 1),
(3, 'Exam', 0),
(4, 'Report', 1),
(5, 'Poster', 1),
(6, 'Portfolio', 1),
(7, 'Video Presentation', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `ID` int(11) NOT NULL,
  `Email` text NOT NULL,
  `Password` text NOT NULL,
  `Surname` text NOT NULL,
  `OtherNames` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`ID`, `Email`, `Password`, `Surname`, `OtherNames`) VALUES
(1, 'staff1@hull.ac.uk', 'dd4d0821fdb71ccb388afe050abf9832', 'Surname', 'Mr Staff'),
(2, 'student1@hull.ac.uk', 'dd4d0821fdb71ccb388afe050abf9832', 'Surname', 'Mr Student'),
(3, 'staff2@hull.ac.uk', 'dd4d0821fdb71ccb388afe050abf9832', 'Surname', 'Mr Reviewer'),
(4, 'm.brown@2016.hull.ac.uk', 'dd4d0821fdb71ccb388afe050abf9832', 'Brown', 'Matthew'),
(5, 'john.whelan@hull.ac.uk', 'dd4d0821fdb71ccb388afe050abf9832', 'Whelan', 'Dr. John');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `coursework`
--
ALTER TABLE `coursework`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `deliverable`
--
ALTER TABLE `deliverable`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `module_student`
--
ALTER TABLE `module_student`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`ParentID`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`ParentID`);

--
-- Indexes for table `submission`
--
ALTER TABLE `submission`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `task_types`
--
ALTER TABLE `task_types`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `coursework`
--
ALTER TABLE `coursework`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `deliverable`
--
ALTER TABLE `deliverable`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `module`
--
ALTER TABLE `module`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8141;
--
-- AUTO_INCREMENT for table `module_student`
--
ALTER TABLE `module_student`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `submission`
--
ALTER TABLE `submission`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `task`
--
ALTER TABLE `task`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `task_types`
--
ALTER TABLE `task_types`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `type`
--
ALTER TABLE `type`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
