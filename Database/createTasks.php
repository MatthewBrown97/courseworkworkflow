<?php


//session_start();


/*
set_time_limit(0); //runs forever
while(true) {
    createTasks();
    sleep(30000);
}
*/

createTasks();

function createTasks(){
	include 'Base.php';
	$link = mysqli_connect($hostname, $username, $password, $dbName);

	// Check connection
	if ($link === false) {
	    die("ERROR: Could not connect. " . mysqli_connect_error());
	}

	//deliverable submission date check for students < 4 days
    $sql = "SELECT deliverable.ID AS 'RelatedDeliverable', 
    (SELECT Description from type where type.ID = deliverable.ID) AS 'Description',
    (SELECT GROUP_CONCAT(StudentID) FROM module_student WHERE ModuleID = (SELECT Module from coursework where find_in_set(deliverable.ID, Deliverables) ) ) AS 'RelatedUsers' ,
    (SELECT GROUP_CONCAT(Title) from coursework where find_in_set(deliverable.ID, Deliverables)) AS 'Coursework', 
    deliverable.SubmissionDate AS 'RelatedDate' 

    from deliverable 
    WHERE 1=1 
    AND DATEDIFF(deliverable.SubmissionDate, CURDATE()) <= 3 
    AND deliverable.SubmissionDate > CURDATE() 
    AND deliverable.ID NOT IN(SELECT GROUP_CONCAT(task.RelatedDeliverable) from task where task.Type = 1 group by ID)";

    //echo $sql;
     if ($result = mysqli_query($link, $sql)) {
        if (mysqli_num_rows($result) > 0) {
            while ($row = $result->fetch_array(MYSQL_ASSOC)) {
                $myArray[] = $row;
                $insertSql = "INSERT INTO task (State, Description, RelatedUsers, ExpiryDate, RelatedDeliverable, Type)VALUES (1, '".$row['Description']."', '".$row['RelatedUsers']."', '".$row['RelatedDate']."', ".$row['RelatedDeliverable'].", 1)";
				//echo $insertSql;
                mysqli_query($link,$insertSql) 
				or die(mysqli_error($link));
            }


        }
    }

    //deliverable submission date check for students 3 - 11 days
    $sql = "SELECT deliverable.ID AS 'RelatedDeliverable', 
    (SELECT Description from type where type.ID = deliverable.ID) AS 'Description',
    (SELECT GROUP_CONCAT(StudentID) FROM module_student WHERE ModuleID = (SELECT Module from coursework where find_in_set(deliverable.ID, Deliverables) ) ) AS 'RelatedUsers' ,
    (SELECT GROUP_CONCAT(Title) from coursework where find_in_set(deliverable.ID, Deliverables)) AS 'Coursework', 
    deliverable.SubmissionDate AS 'RelatedDate' 

    from deliverable 
    WHERE 1=1 
    AND DATEDIFF(deliverable.SubmissionDate, CURDATE()) > 3 
    AND DATEDIFF(deliverable.SubmissionDate, CURDATE()) < 11 
    AND deliverable.SubmissionDate > CURDATE() 
    AND deliverable.ID NOT IN(SELECT GROUP_CONCAT(task.RelatedDeliverable) from task where task.Type = 1 group by ID)";

    //echo $sql;
     if ($result = mysqli_query($link, $sql)) {
        if (mysqli_num_rows($result) > 0) {
            while ($row = $result->fetch_array(MYSQL_ASSOC)) {
                $myArray[] = $row;
                $insertSql = "INSERT INTO task (State, Description, RelatedUsers, ExpiryDate, RelatedDeliverable, Type)VALUES (2, '".$row['Description']."', '".$row['RelatedUsers']."', '".$row['RelatedDate']."', ".$row['RelatedDeliverable'].", 1)";
				//echo $insertSql;
                mysqli_query($link,$insertSql) 
				or die(mysqli_error($link));
            }


        }
    }

    //check feedback date for deliverables < 4 days away
    $sql = "SELECT deliverable.ID AS 'RelatedDeliverable', 
			(SELECT Description from type where type.ID = deliverable.ID) AS 'Description', 
			(SELECT GROUP_CONCAT(ParentId) FROM staff where find_in_set( (SELECT Module from coursework where find_in_set(deliverable.ID, Deliverables)), CONCAT(Modules,ModulesManaged)) ) AS 'RelatedUsers' , 
			 (SELECT GROUP_CONCAT(Title) from coursework where find_in_set(deliverable.ID, Deliverables)) AS 'Coursework', 
			 deliverable.FeedbackDate AS 'RelatedDate' 
			 
			 from deliverable 
			 WHERE 1=1 
             AND DATEDIFF(deliverable.FeedbackDate, CURDATE()) <= 4 
			 AND deliverable.FeedbackDate > CURDATE() 
			 AND deliverable.ID NOT IN(SELECT GROUP_CONCAT(task.RelatedDeliverable) from task where task.Type = 2 group by ID)";

    //echo $sql;
    
	if ($result = mysqli_query($link, $sql)) {
		if (mysqli_num_rows($result) > 0) {
		    while ($row = $result->fetch_array(MYSQL_ASSOC)) {
		        $myArray[] = $row;
		        
		        $insertSql = "INSERT INTO task (State, Description, RelatedUsers, ExpiryDate, RelatedDeliverable, Type)VALUES (1, '".$row['Description']."', '".$row['RelatedUsers']."', '".$row['RelatedDate']."', ".$row['RelatedDeliverable'].", 2)";
				//echo $insertSql;
		        mysqli_query($link,$insertSql) 
				or die(mysqli_error($link));
				
		    }

		}
	}

	//check feedback date for deliverables  4 > days away < 11
    $sql = "SELECT deliverable.ID AS 'RelatedDeliverable', 
			(SELECT Description from type where type.ID = deliverable.ID) AS 'Description', 
			(SELECT GROUP_CONCAT(ParentId) FROM staff where find_in_set( (SELECT Module from coursework where find_in_set(deliverable.ID, Deliverables)), CONCAT(Modules,ModulesManaged)) ) AS 'RelatedUsers' , 
			 (SELECT GROUP_CONCAT(Title) from coursework where find_in_set(deliverable.ID, Deliverables)) AS 'Coursework', 
			 deliverable.FeedbackDate AS 'RelatedDate' 
			 
			 from deliverable 
			 WHERE 1=1 
             AND DATEDIFF(deliverable.FeedbackDate, CURDATE()) > 4 
             AND DATEDIFF(deliverable.FeedbackDate, CURDATE()) < 11
			 AND deliverable.FeedbackDate > CURDATE() 
			 AND deliverable.ID NOT IN(SELECT GROUP_CONCAT(task.RelatedDeliverable) from task where task.Type = 2 group by ID)";

    //echo $sql;
    
	if ($result = mysqli_query($link, $sql)) {
		if (mysqli_num_rows($result) > 0) {
		    while ($row = $result->fetch_array(MYSQL_ASSOC)) {
		        $myArray[] = $row;
		        
		        $insertSql = "INSERT INTO task (State, Description, RelatedUsers, ExpiryDate, RelatedDeliverable, Type)VALUES (2, '".$row['Description']."', '".$row['RelatedUsers']."', '".$row['RelatedDate']."', ".$row['RelatedDeliverable'].", 2)";
				//echo $insertSql;
		        mysqli_query($link,$insertSql) 
				or die(mysqli_error($link));
				
		    }

		}
	}
	
	//check feedback for review failed
    $sql = "SELECT Coursework AS 'RelatedCoursework',
			ReviewDeadline AS 'RelatedDate',
			(SELECT Title from coursework WHERE coursework.ID = Coursework) AS 'Description',
			(SELECT Reviewer from module WHERE module.ID = (SELECT Module from coursework where coursework.ID = Coursework )) AS 'RelatedUsers'
			from review 
			
			WHERE Passed = 0
			
            
			AND Coursework NOT IN(SELECT GROUP_CONCAT(task.RelatedCoursework) from task where task.Type = 3 group by ID)";

    //echo $sql;
    
	if ($result = mysqli_query($link, $sql)) {
		if (mysqli_num_rows($result) > 0) {
		    while ($row = $result->fetch_array(MYSQL_ASSOC)) {
		        $myArray[] = $row;
		        
		        $insertSql = "INSERT INTO task (State, Description, RelatedUsers, ExpiryDate, RelatedCoursework, Type)
				VALUES (2, '".$row['Description']. " Review Failed', '".$row['RelatedUsers']."', '".$row['RelatedDate']."', ".$row['RelatedCoursework'].", 3)";
				//echo $insertSql;
		        mysqli_query($link,$insertSql) 
				or die(mysqli_error($link));
				
		    }

		}
	}

	//daily check for coursework released
    $sql = "SELECT ID AS 'RelatedCoursework',
    		Title AS 'Description',
    		ReleaseDate AS 'RelatedDate',
    		(SELECT GROUP_CONCAT(StudentID) FROM module_student WHERE ModuleID = Module) AS 'RelatedUsers'
			from coursework 
			where 
			ID NOT IN(SELECT GROUP_CONCAT(task.RelatedCoursework) from task where task.Type = 4 group by ID)
			AND DATEDIFF(ReleaseDate, CURDATE()) = 0";

    //echo $sql;
    
	if ($result = mysqli_query($link, $sql)) {
		if (mysqli_num_rows($result) > 0) {
		    while ($row = $result->fetch_array(MYSQL_ASSOC)) {
		        $myArray[] = $row;
		        
		        $insertSql = "INSERT INTO task (State, Description, RelatedUsers, ExpiryDate, RelatedCoursework, Type, Message)
				VALUES (2, '".$row['Description']."', '".$row['RelatedUsers']."', '".$row['RelatedDate']."', ".$row['RelatedCoursework'].", 4, 'This coursework has been released today')";
				//echo $insertSql;
		        mysqli_query($link,$insertSql) 
				or die(mysqli_error($link));
				
		    }

		}
	}


	//echo json_encode($myArray);

    //mysqli_free_result($result);
	

}




?>