<?php

include 'Base.php';

$link = mysqli_connect($hostname, $username, $password, $dbName);

// Check connection
if ($link === false) {
    die("ERROR: Could not connect. " . mysqli_connect_error());
}

// Attempt select query execution
echo "Users";
$sql = "SELECT * FROM user";
if ($result = mysqli_query($link, $sql)) {
    if (mysqli_num_rows($result) > 0) {
        echo "<table>";
        echo "<tr>";
        echo "<th>ID</th>";
        echo "<th>Email</th>";
        echo "<th>Password</th>";
        echo "<th>OtherNames</th>";
        echo "<th>Surname</th>";
        echo "</tr>";
        while ($row = mysqli_fetch_array($result)) {
            echo "<tr>";
            echo "<td>" . $row['ID'] . "</td>";
            echo "<td>" . $row['Email'] . "</td>";
            echo "<td>" . $row['Password'] . "</td>";
            echo "<td>" . $row['OtherNames'] . "</td>";
            echo "<td>" . $row['Surname'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";
        // Free result set
        mysqli_free_result($result);
    } else {
        echo "No records matching your query were found.";
    }
} else {
    echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
}

$sql2 = "SELECT ID, (SELECT Name from module WHERE Module = module.ID) AS 'Module', ReleaseDate, FeedbackDate, (SELECT OtherNames FROM user WHERE ID = (SELECT Reviewer FROM module WHERE module.ID = coursework.Module)) AS 'Reviewer',
(SELECT GROUP_CONCAT(Description) from type WHERE find_in_set(type.ID,
    (SELECT GROUP_CONCAT(Type) FROM deliverable WHERE find_in_set(deliverable.ID,(SELECT Deliverables) ) ) )
) AS 'Deliverables' from coursework";
echo "Courseworks";
if ($result = mysqli_query($link, $sql2)) {
    if (mysqli_num_rows($result) > 0) {
        echo "<table>";
        echo "<tr>";
        echo "<th>ID</th>";
        echo "<th>Module</th>";
        echo "<th>ReleaseDate</th>";
        echo "<th>FeedbackDate</th>";
        echo "<th>Reviewer</th>";
        echo "<th>Deliverables</th>";
        echo "</tr>";
        while ($row = mysqli_fetch_array($result)) {
            echo "<tr>";
            echo "<td>" . $row['ID'] . "</td>";
            echo "<td>" . $row['Module'] . "</td>";
            echo "<td>" . $row['ReleaseDate'] . "</td>";
            echo "<td>" . $row['FeedbackDate'] . "</td>";
            echo "<td>" . $row['Reviewer'] . "</td>";
            echo "<td>" . $row['Deliverables'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";
        // Free result set
        mysqli_free_result($result);
    } else {
        echo "No records matching your query were found.";
    }
} else {
    echo "ERROR: Could not able to execute $sql2. " . mysqli_error($link);
}

$sql3 = "SELECT student.ParentID AS 'ID', user.OtherNames AS 'OtherNames', user.Surname AS 'Surname', user.Email AS 'Email' from student INNER JOIN user ON student.ParentID=user.ID";
echo 'Students';
if ($result = mysqli_query($link, $sql3)) {
    if (mysqli_num_rows($result) > 0) {
        echo "<table>";
        echo "<tr>";
        echo "<th>ID</th>";
        echo "<th>OtherNames</th>";
        echo "<th>Surname</th>";
        echo "<th>Email</th>";
        echo "</tr>";
        while ($row = mysqli_fetch_array($result)) {
            echo "<tr>";
            echo "<td>" . $row['ID'] . "</td>";
            echo "<td>" . $row['OtherNames'] . "</td>";
            echo "<td>" . $row['Surname'] . "</td>";
            echo "<td>" . $row['Email'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";
        // Free result set
        mysqli_free_result($result);
    } else {
        echo "No records matching your query were found.";
    }
} else {
    echo "ERROR: Could not able to execute $sql3. " . mysqli_error($link);
}

$sql4 = "SELECT staff.ParentID AS 'ID', user.OtherNames AS 'OtherNames', user.Surname AS 'Surname', user.Email AS 'Email' from staff INNER JOIN user ON staff.ParentID=user.ID";
echo 'Staff';
if ($result = mysqli_query($link, $sql4)) {
    if (mysqli_num_rows($result) > 0) {
        echo "<table>";
        echo "<tr>";
        echo "<th>ID</th>";
        echo "<th>OtherNames</th>";
        echo "<th>Surname</th>";
        echo "<th>Email</th>";
        echo "</tr>";
        while ($row = mysqli_fetch_array($result)) {
            echo "<tr>";
            echo "<td>" . $row['ID'] . "</td>";
            echo "<td>" . $row['OtherNames'] . "</td>";
            echo "<td>" . $row['Surname'] . "</td>";
            echo "<td>" . $row['Email'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";
        // Free result set
        mysqli_free_result($result);
    } else {
        echo "No records matching your query were found.";
    }
} else {
    echo "ERROR: Could not able to execute $sql4. " . mysqli_error($link);
}

// Close connection
mysqli_close($link);
