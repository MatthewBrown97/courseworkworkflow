<?php
include '../../Database/login-check.php';
echo '<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Coursework</title>
        <link rel="stylesheet" href="style.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
        <script src="script.js"></script>

        <script>
        function getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return \'\';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

        function checkParamExists(param,value){
                if(value != "" && value != null){
                    var string = param+value;
                    return string;
                } else {
                    return "";
                }
        }
        </script>';

echo '<script type="text/javascript">
        $("document").ready(function(){
            var ModuleCode = {
                    "ModuleCode": getParameterByName(\'ModuleCode\')
                };
           $.ajax({
                type: "get",
                url: "../../Database/GetFromDatabase.php",
                data: "Field=Coursework"+checkParamExists("&ModuleCode=",getParameterByName("ModuleCode")),
                dataType: "json",
                success: function(response) {

                    var input_wrapper = "";
                    var toggleInt = 0;
                    var arrayInt = 0;
                    if(response!=null){
                        Object.keys(response).forEach(function(key) {

                            //console.log(key, obj[key]);
                            var Title = response[key].Title;
                            var ID = response[key].ID;
                            var Deliverables = response[key].Deliverables;
                            var CourseworkReady = response[key].CourseworkReady;
                            var ReleaseDate = new Date(response[key].ReleaseDate).toDateString();


                            if(CourseworkReady == "1"){
                                CourseworkReady = "READY Releasing on: "+ReleaseDate;
                            } else {
                                CourseworkReady = "NOT READY";
                            }

                            input_wrapper += "<input id=\'toggle"+toggleInt+"\' class=\'toggle\' type=\'checkbox\' onclick=\'Toggling(id)\'/> <label class=\'toggle-label\' id=\'label"+toggleInt+"\' for=\'toggle"+toggleInt+"\'> <h3>"+Title+" ("+getParameterByName("ModuleCode")+") "+CourseworkReady+"</h3> </label><div class=\'collapse-wrapper\' id=\'collapse"+toggleInt+"\'> <div id=\'placeholder"+toggleInt+"\'>Loading...</div>";

                            $("#toggle"+toggleInt).live("click", function() {
                                var clickedBtnID = $(this).attr(\'id\');
                                var Id = clickedBtnID.replace(/\D/g,\'\');
                               $.ajax({
                                    type: "get",
                                    url: "../../Database/GetFromDatabase.php",
                                    data: "Field=Deliverables&Deliverables="+Deliverables,
                                    dataType: "json",
                                    success: function(deliverables) {
                                        var table_wrapper = "<table><tr><th>Deliverable Name</th><th>ID</th><th>Submission Date</th><th>Type</th></tr>";
                                        Object.keys(deliverables).forEach(function(key) {

                                            //console.log(key, obj[key]);
                                            var Name = deliverables[key].Name;
                                            var ID = deliverables[key].ID;

                                            var SubmissionDate = deliverables[key].SubmissionDate.replace(/^(\d+)-(\d+)-(\d+)(.*):\d+$/, \'$3/$2/$1$4\');;

                                            var Type = deliverables[key].Type;

                                            table_wrapper += "<tr id=\'row"+ID+"\' onclick=\'EditDeliverable("+ID+")\'><td>"+Name+"</td><td>"+ID+"</td><td>"+SubmissionDate+"</td><td>"+Type+"</td></tr>";
                                        })

                                        document.getElementById("placeholder"+Id).innerHTML = table_wrapper;
                                    }
                                });
                           });

                            input_wrapper += "</table><input type=\'button\' value=\'View Coursework Specification\' onclick=\'GoToSpecView("+ID+")\'></div>";

                            toggleInt++;

                        });

                    }

                    document.getElementById("response").innerHTML = input_wrapper;
                }
            });

        });







        </script>';

echo '</head>
    <body>
        <header>
            <h1>Coursework</h1>
        </header>';

include 'navbar.php';

echo '      <div class="content">
             
        <div class="large-col">';

echo '<p id="response">Loading...</p>';

echo '
        </div>

    </body>
</html>';
