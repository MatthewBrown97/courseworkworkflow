<?php
include '../../Database/login-check.php';
echo '<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Edit Deliverable</title>
        <link rel="stylesheet" href="style.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
        <script src="script.js"></script>
        <script>
        function getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return \'\';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

        function checkParamExists(param,value){
                if(value != "" && value != null){
                    var string = param+value;
                    return string;
                } else {
                    return "";
                }
        }
        </script>';

echo '<script type="text/javascript">
        $("document").ready(function(){
           $.ajax({
                type: "get",
                url: "../../Database/GetFromDatabase.php",
                data: "Field=Deliverable"+checkParamExists("&DeliverableId=",getParameterByName("DeliverableId")),
                dataType: "json",
                success: function(response) {

                    var input_wrapper = "";
                    var toggleInt = 0;
                    var arrayInt = 0;
                    if(response!=null){
                        Object.keys(response).forEach(function(key) {

                            var Desc = response[key].Description;
                            var SubmissionDate = response[key].SubmissionDate;
                            var tzoffset = (new Date()).getTimezoneOffset() * 60000;
                            var date = new Date(new Date(SubmissionDate)-tzoffset).toISOString();
                            SubmissionDate =date.slice(0, -1);
                            
                            var Types = response[key].Types;

                            input_wrapper += \'<form class="centered-form"><label class="left-aligned-label" for="desc" >Description</label><br><input class="med-input" name="desc" type="text" value="\'+Desc+\'"/><br><label class="left-aligned-label" for="submission-date">Submission Date</label><br><input class="med-input" name="submission-date" type="datetime-local" value="\'+SubmissionDate+\'"/><br><label class="left-aligned-label" for="type">Type</label><br><select class="med-input" name="type" placeholder="Type"><option selected disabled hidden>Select a Type</option>\';
                                                    

                            var options = Types.split(",");
                            for(i=0; i<options.length; i++){
                                input_wrapper += \'<option\';
                                if(options[i] == Desc){
                                        input_wrapper += \' selected \';
                                }
                                input_wrapper += \' >\'+options[i]+\'</option>\';
                            }

                            input_wrapper += \'</select><br><input class="toggle" id="toggle"  type="checkbox" onclick="Toggling(id)"/><label class="centered-label" id="label" for="toggle">Create New Type</label><div class="blank-collapse-wrapper" id="collapse"><input class="med-input" type="text" placeholder="Type Name..."/></div><input type="button" value="Update"/></form>\';

                            toggleInt++;

                        });

                    }

                    document.getElementById("response").innerHTML = input_wrapper;
                }
            });

        });
        </script>';


echo'</head>
    <body>
        <header>
            <h1>Edit Deliverable</h1>
        </header>';

include 'navbar.php';

echo '
        <div class="small-content">
            <div class="full-col">
                
                    <p id="response">Loading...</p>

            </div>
        </div>
    </body>
</html>';
