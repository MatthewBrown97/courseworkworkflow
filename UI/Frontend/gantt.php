<?php
include '../../Database/login-check.php';
echo '

<!DOCTYPE html>
 
<html>
<head>
<meta charset="UTF-8"/>
        <title>Modules View</title>
        <link rel="stylesheet" href="style.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
        <script src="script.js"></script>

        <script>
        function addCoursework(releaseDate, submissionDate, feedbackDate, coursework){
        	var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
			var daysInMonth = [31,28,31,30,31,30,31,31,30,31,30,31];

			if(releaseDate !=null && submissionDate != null){
				var releaseMonth = releaseDate.getMonth()+1;
        		var releaseDay = releaseDate.getDate();

        		var submissionMonth = submissionDate.getMonth()+1;
        		var submissionDay = submissionDate.getDate();


        		
        		while(releaseMonth <= submissionMonth){
        		
        			while(!(releaseDay == submissionDay && releaseMonth == submissionMonth)){
						if(releaseDay < daysInMonth[releaseMonth]){
							document.getElementById("coursework:"+coursework+";month:"+releaseMonth+";day:"+releaseDay).parentNode.style.background = "green";
							document.getElementById("coursework:"+coursework+";month:"+releaseMonth+";day:"+releaseDay).style.background = "green";

						} else {
							releaseDay = -1;
							releaseMonth++;
						}

						releaseDay++;
					}
					releaseMonth++;
        		}
        		
        		
        		

			}

			/*
        	if(releaseDate !=null){
        		var releaseMonth = releaseDate.getMonth()+1;
        		var releaseDay = releaseDate.getDate();
        		document.getElementById("coursework:"+coursework+";month:"+releaseMonth+";day:"+releaseDay).parentNode.style.background = "green";
				document.getElementById("coursework:"+coursework+";month:"+releaseMonth+";day:"+releaseDay).style.background = "green";
        	}
        	*/

        	if(submissionDate != null){
        		var submissionMonth = submissionDate.getMonth()+1;
        		var submissionDay = submissionDate.getDate();
        		document.getElementById("coursework:"+coursework+";month:"+submissionMonth+";day:"+submissionDay).parentNode.style.background = "red";
				document.getElementById("coursework:"+coursework+";month:"+submissionMonth+";day:"+submissionDay).style.background = "red";
        	}


			if(feedbackDate != null){
				var feebackMonth = feedbackDate.getMonth()+1;
				var feebackDay = feedbackDate.getDate();
				document.getElementById("coursework:"+coursework+";month:"+feebackMonth+";day:"+feebackDay).parentNode.style.background = "blue";
				document.getElementById("coursework:"+coursework+";month:"+feebackMonth+";day:"+feebackDay).style.background = "blue";

			}
		
		
	}
	</script>';

echo '<script type="text/javascript">
        $("document").ready(function(){
           $.ajax({
                type: "get",
                url: "../../Database/GetFromDatabase.php",
                data: "Field=Date",
                dataType: "json",
                success: function(response) {
                    if(response!=null){
                    	var courseworks=[];
                    	var dates =[];
                    	var courseworkInt = 0;
                        Object.keys(response).forEach(function(key) {

                            var ReleaseDate = response[key].ReleaseDate;
                            var SubmissionDate = response[key].SubmissionDate;
                            var FeedbackDate = response[key].FeedbackDate;
                            var Coursework = response[key].Coursework;
                            var CourseworkTitle = response[key].CourseworkTitle;
                            courseworks.push(CourseworkTitle);

                            if(SubmissionDate != null){
								SubmissionDate = new Date(SubmissionDate);
                            }
                            if(FeedbackDate != null){
                            	FeedbackDate = new Date(FeedbackDate);
                            }
                            if(ReleaseDate != null){
                            	ReleaseDate = new Date(ReleaseDate);
                            }

                            dates.push([ReleaseDate, SubmissionDate, FeedbackDate, courseworkInt]);

                            
                            courseworkInt++;
                        });

                        RenderGanttChart(courseworks);

                        for (var i = 0; i < dates.length; i++) {
						    addCoursework(dates[i][0],dates[i][1],dates[i][2],dates[i][3]);
						}

                        //addCoursework(ReleaseDate,SubmissionDate,FeedbackDate,Coursework);

                    }

                }
            });

        });
        </script>';


echo'
<script>

function RenderGanttChart(Courseworks){
	var HTML = \'<table id="chart" style="width:100%; border:1px solid black">\';
	var monthsInSemester = 3;

	var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
	var daysInMonth = [31,28,31,30,31,30,31,31,30,31,30,31];

	var numberOfCourseworks = Courseworks.length-1;

	var totalRows = 12/monthsInSemester;
	for(var row = 0; row < totalRows; row++){

		HTML+= \'<th style="height:50px; width:155px">Coursework</th>\';

		//$monthsInSemester++;
		
		if(row != 0){
			var month = row*monthsInSemester;
			var finalMonthInRow = (row*monthsInSemester)+monthsInSemester;
		} else {
			var finalMonthInRow = row+monthsInSemester;
			month = 0;
		}
		while (month < finalMonthInRow) { 
				HTML+= \'<th>\'+months[month]+\'</th>\';
				month++;
			}

		

		for (var coursework=0; coursework <= numberOfCourseworks; coursework++) { 
			HTML+= \'<tr>\';
			HTML+= \'<th style="border:1px solid black; height:50px; width:155px">\'+Courseworks[coursework]+\'</th>\';

			if(row != 0){
				var month = row*monthsInSemester;
				var finalMonthInRow = (row*monthsInSemester)+monthsInSemester;
			} else {
				finalMonthInRow = row+monthsInSemester;
			month = 0;
			}
			while (month < finalMonthInRow) {
			//echo\'Month: \'.month;  
				HTML+= \'<th style="border:1px solid black; ">\';

				HTML+= \'<table>\';
				
				for (var day=0; day < daysInMonth[month] ; day++) { 
					HTML+= \'<th style="border:1px solid black; "><div id="coursework:\'+coursework+\';month:\'+month+\';day:\'+day+\'" style="height:50px; width:1px"></div></th>\';
				}

				HTML+= \'</table>\';

				HTML+= \'</th>\';

				month++;
			}


			HTML+= \'</tr>\';
		}

	
	}

	document.getElementById("chart").innerHTML = HTML+"</table>";
}

	
</script>';
echo '</head>
    <body>
        <header>
            <h1>Calendar</h1>
        </header>';

include 'navbar.php';

echo'
<body>
<p id="chart">Loading...</p>

';


echo '

</body>
</html>

';

?>