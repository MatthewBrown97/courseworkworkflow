<?php
session_start();
$User = null;
if (isset($_SESSION['LoggedInUserId'])) {
    $User = $_SESSION['LoggedInUserId'];
    //echo "User: " . $User;
}
echo '<html>

<head>
    <meta charset="UTF-8" />
    <title>Log in</title>
    <link rel="stylesheet" href="style.css" />
    <script src="script.js" type="text/javascript"></script>
    <!-- Added script src -->
</head>

<body>
    <div id="login-background">
        <div id="spacer-top">
        </div>
        <div id="login-wrapper">
            <form id="login-form" name="login" action="\edsa-courseworkworkflow/Database/GetFromDatabase.php" onsubmit="return Validation()" method="GET">
                <h1>Coursework Portal</h1>
                <input name="username" type="text" placeholder="E-mail" required/>
                <input name="password" type="password" placeholder="Password" required/>
                <p class="error-text" id="errTxt"></p>
                ';

echo "<script>LoggedInCheck('";
if (isset($_SESSION['status'])) {
    echo $_SESSION['status'];
}
echo "');</script>";

echo '
                <button type="submit">Log In</button>
            </form>
            <div id="forgot-pass-link"><a href="forgot-password.html">Forgot Password?</a></div>
        </div>
        <div id="spacer-bottom">
        </div>
    </div>
</body>

</html>'

;
