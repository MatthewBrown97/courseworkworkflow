<?php
include '../../Database/login-check.php';
echo '<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Modules View</title>
        <link rel="stylesheet" href="style.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
        <script src="script.js"></script>';

echo '<script type="text/javascript">
        $("document").ready(function(){
           $.ajax({
                type: "get",
                url: "../../Database/GetFromDatabase.php",
                data: "Field=Module",
                dataType: "json",
                success: function(response) {

                    var Managed = "";
                    var Reviewed = "";
                    var Taking = "";
                    var toggleInt = 0;
                    var arrayInt = 0;
                    if(response!=null){
                        var PercentageReady = "";
                        Object.keys(response).forEach(function(key) {

                            //console.log(key, obj[key]);
                            var ID = response[key].ID;
                            var Name = response[key].Name;
                            var ModuleCode = response[key].ModuleCode;
                            var ModulesManged = response[key].ModulesManaged;
                            var Reviewing = response[key].Reviewing;
                            var Student = response[key].Student;

                            var AllCourseworks = response[key].AllCourseworks;
                            var ReadyCourseworks = response[key].ReadyCourseworks;

                            PercentageReady = ReadyCourseworks+"/"+AllCourseworks;

                            var thisModule = "";

                            if(Student == "TRUE"){
                                thisModule += "<input id=\'toggle"+toggleInt+"\' class=\'toggle\' type=\'checkbox\' onclick=\'Toggling(id)\'/> <label class=\'toggle-label\' id=\'label"+toggleInt+"\' for=\'toggle"+toggleInt+"\'> <h3>"+Name+"</h3> </label><div class=\'collapse-wrapper\' id=\'collapse"+toggleInt+"\'>";
                                thisModule += " <table><tr><th id=\'acw-wrapper-text\'>Module Code:</th></tr><tr><td id=\'acw-wrapper-text\'>"+ModuleCode+"</td></tr></table>";

                                thisModule += "<div class=\'full-col-inner\'><input type=\'button\' value=\'View Courseworks\' onclick=\'GoToCourseworkView(this.parentElement)\'></div><div class=\'full-col-inner\'><input type=\'button\' value=\'View People\' onclick=\'GoToPeopleView()\'></div></div>";

                                Taking += thisModule;

                            } else {
                                thisModule += "<input id=\'toggle"+toggleInt+"\' class=\'toggle\' type=\'checkbox\' onclick=\'Toggling(id)\'/> <label class=\'toggle-label\' id=\'label"+toggleInt+"\' for=\'toggle"+toggleInt+"\'> <h3>"+Name+"</h3> </label><div class=\'collapse-wrapper\' id=\'collapse"+toggleInt+"\'>";
                                thisModule += " <table><tr><th id=\'acw-wrapper-text\'>Module Code:</th></tr><tr><td id=\'acw-wrapper-text\'>"+ModuleCode+"</td></tr></table>";

                                thisModule += "<div class=\'full-col-inner\'><input type=\'button\' value=\'View Courseworks\' onclick=\'GoToCourseworkView(this.parentElement)\'></div><div class=\'full-col-inner\'><input type=\'button\' value=\'View People\' onclick=\'GoToPeopleView()\'></div></div>";
                                if(ModulesManged.includes(ID)){
                                    Managed += thisModule;
                                } else if (Reviewing.includes(ID)){
                                    Reviewed+= thisModule;
                                }
                            }
                            

                            

                            toggleInt++;

                        });

                    }

                    if(Taking != ""){
                         
                         document.getElementById("acw-wrapper").innerHTML = \'<div id="acw-wrapper">\'+Taking+\'</div>\';
                    } else {
                        document.getElementById("Managed").innerHTML = Managed;
                        document.getElementById("Reviewed").innerHTML = Reviewed;
                    }

                    if(PercentageReady != ""){
                        document.getElementById("ManagedHeading").innerHTML = "Managed ("+PercentageReady+" Ready)";
                    }

                    

                }
            });

        });
        </script>';

echo '</head>
    <body>
        <header>
            <h1>Modules</h1>
        </header>';

include 'navbar.php';

echo '
        <div class="small-content">
            <div class="big-col">
                <div id="acw-wrapper">
                    <h1 id="ManagedHeading">Managed</h1>
                	<p id="Managed">Loading...</p>
                    <h1>Reviewing</h1>
                    <p id="Reviewed"></p>
                </div>
            </div>
        </div>
    </body>
</html>';
