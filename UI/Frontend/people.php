<?php
include '../../Database/login-check.php';
echo '<html>
    <head>
        <meta charset="UTF-8"/>
        <title>People</title>
        <link rel="stylesheet" href="style.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
        <script src="script.js"></script>';

echo '<script type="text/javascript">
        $("document").ready(function(){
           $.ajax({
                type: "get",
                url: "../../Database/GetFromDatabase.php",
                data: "Field=Users",
                dataType: "json",
                success: function(response) {

                    var input_wrapper = "<table id=\'person-table\'><tr><th>Name</th> <th>E-Mail</th> <th>Role</th><th>ID</th></tr>";
                    var toggleInt = 0;
                    var arrayInt = 0;
                    if(response!=null){
                        Object.keys(response).forEach(function(key) {

                            var ID = response[key].ID;
                            var Name = response[key].OtherNames;
                            var Email = response[key].Email;

                            input_wrapper += "<tr><td>"+Name+"</td><td>"+Email+"</td><td>Role</td><td>"+ID+"</td></tr>";

                            toggleInt++;

                        });

                    }

                    document.getElementById("response").innerHTML = input_wrapper;
                }
            });

        });
        </script>';

echo '
    </head>
    <body>
        <header>
            <h1>People</h1>
        </header>';

include 'navbar.php';

echo '
        <div class="query-bar">
            <form>
                <input type="text" id="search" onkeyup="Filter(id)" placeholder="Search..."/>
                <select id="role" class="small-input" onchange="Filter(id)">
                    <option value="role" selected>Role</option>
                    <option value="student">Students</option>
                    <option value="staff">Staff</option>
                    <option value="reviewer">Reviewers</option>                    
                </select>
                <button type="button"><img src="src/search-icon.svg"/></button>
            </form>
        </div>
        <div class="content">
            ';
echo '<p id="response">Loading...</p>';
echo '
        </div>
    </body>
</html>';
