<?php
include '../../Database/login-check.php';
echo '<html>
    <head>
        <meta charset="UTF-8">
        <title>Review Form</title>
        <link rel="stylesheet" href="style.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
        <script src="script.js"></script>

         <script>
        function getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return \'\';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

        function checkParamExists(param,value){
                if(value != "" && value != null){
                    var string = param+value;
                    return string;
                } else {
                    return "";
                }
        }
        </script>
        ';
echo '<script type="text/javascript">
        $("document").ready(function(){
           $.ajax({
                type: "get",
                url: "../../Database/GetFromDatabase.php",
                data: "Field=ReviewForm"+checkParamExists("&CourseworkID=",getParameterByName("CourseworkID")),
                dataType: "json",
                success: function(response) {

                    if(response!=null){
                        var PercentageReady = "";
                        Object.keys(response).forEach(function(key) {

                            //console.log(key, obj[key]);
                            var Title = response[key].Title;
                            var AssesmentCode = response[key].AssesmentCode;
                            var Reviewer = response[key].Reviewer;
                            var WeightingSizeLengthMatch = response[key].WeightingSizeLengthMatch;
                            var LearningOutcomesFulfilled = response[key].LearningOutcomesFulfilled;
                            var Clear = response[key].Clear;

                            var SuitableTimescale = response[key].SuitableTimescale;
                            var AppropriateLevel = response[key].AppropriateLevel;

                            var AppropriateMarkScheme = response[key].AppropriateMarkScheme;
                            var ConsistentMarkScheme = response[key].ConsistentMarkScheme;
                            var AcademicMisconductLimited = response[key].AcademicMisconductLimited;
                            var SecondMarking = response[key].SecondMarking;
                            var EthicalApproval = response[key].EthicalApproval;
                            var RiskAssesment = response[key].RiskAssesment;

                            var SameReassessment = response[key].SameReassessment;
                            var Passed = response[key].Passed;


                            

                            document.getElementById("Title").value = Title;
                            document.getElementById("AssesmentCode").value = AssesmentCode;
                            document.getElementById("Reviewer").value = Reviewer;


                            document.getElementById("WeightingSizeLengthMatch"+WeightingSizeLengthMatch).checked = true;

                            
                            document.getElementById("LearningOutcomesFulfilled"+LearningOutcomesFulfilled).checked = true;
                            document.getElementById("Clear"+Clear).checked = true;
                            document.getElementById("SuitableTimescale"+SuitableTimescale).checked = true;
                            document.getElementById("AppropriateLevel"+AppropriateLevel).checked = true;
                            document.getElementById("AppropriateMarkScheme"+AppropriateMarkScheme).checked = true;
                            document.getElementById("ConsistentMarkScheme"+ConsistentMarkScheme).checked = true;
                            document.getElementById("AcademicMisconductLimited"+AcademicMisconductLimited).checked = true;
                            document.getElementById("SecondMarking"+SecondMarking).checked = true;
                            document.getElementById("EthicalApproval"+EthicalApproval).checked = true;
                            document.getElementById("RiskAssesment"+RiskAssesment).checked = true;
                            document.getElementById("SameReassessment"+SameReassessment).checked = true;
                            
                        });

                    }                

                }
            });

        });
        </script>';

echo '</head>
    <body>
        <header>
            <h1>Review Form</h1>
        </header>';


include 'navbar.php';

echo '
        <div class="small-content">
		<form action="\edsa-courseworkworkflow/Database/PutToDatabase.php" onsubmit="return RadioValidation()" method="GET">
            <table id="review-table">
				<tr>
					<td colspan="4"><input type="text" placeholder="Module Number, Level, Name: " class="med-input" required/></td>
				</tr>
				<tr>
					<td colspan="4"><input type="text" id="Title" placeholder="Title of Assessment:" class="med-input" required/></td>
				</tr>
				<tr>
					<td colspan="4"><input type="text" id="AssesmentCode" placeholder="Assessment Code:" class="med-input" required/></td>
				</tr>
				<tr>
					<td colspan="4"><input type="text" placeholder="Assessment Setter:" class="med-input" required/></td>
				</tr>
				<tr>
				<td colspan="4"><input type="text" id="Reviewer" placeholder="Internal Reviewers:" class="med-input" required/></td>
				</tr>
				<tr>
					<td colspan="4"><input type="text" placeholder="Assessment Weighting towards Module Mark:" class="med-input" required/></td>
				</tr>
				<tr id="multi-choice-row"> 
					<th></th>
					<th>Yes</th>
					<th>No</th>
					<th>N/A</th>
				</tr>
				<tr id="answer0">
					<td>Does the assessment match the weighting and size/length as published in
the module specification and CANVAS?</td>
					<td><input id="WeightingSizeLengthMatch1" type="radio" onclick="RadioCheck(this)"/></td>
					<td><input id="WeightingSizeLengthMatch0" type="radio" onclick="RadioCheck(this)"/></td>
					<td><input id="WeightingSizeLengthMatchnull" type="radio" onclick="RadioCheck(this)"/></td>
				</tr>
				<tr id="multi-choice-row"> 
					<th></th>
					<th>Yes</th>
					<th>No</th>
					<th>N/A</th>
				</tr>
				<tr id="answer2">
					<td>Does the assessment address the relevant learning outcomes stated in the
module specification? If appropriate, are the learning outcomes identified?</td>
					<td><input id="LearningOutcomesFulfilled1" type="radio" onclick="RadioCheck(this)"/></td>
					<td><input id="LearningOutcomesFulfilled0" type="radio" onclick="RadioCheck(this)"/></td>
					<td><input id="LearningOutcomesFulfillednull" type="radio" onclick="RadioCheck(this)"/></td>
				</tr>
				<tr id="multi-choice-row"> 
					<th></th>
					<th>Yes</th>
					<th>No</th>
					<th>N/A</th>
				</tr>
				<tr id="answer3">
					<td>Is the assessment clear and unambiguous?</td>
					<td><input type="radio" id="Clear1" onclick="RadioCheck(this)"/></td>
					<td><input type="radio" id="Clear0" onclick="RadioCheck(this)"/></td>
					<td><input type="radio" id="Clearnull" onclick="RadioCheck(this)"/></td>
				</tr>
				<tr id="multi-choice-row"> 
					<th></th>
					<th>Yes</th>
					<th>No</th>
					<th>N/A</th>
				</tr>
				<tr id="answer4">
					<td>Does the assessment appear to be at an appropriate level within the FHEQ?</td>
					<td><input type="radio" id="SuitableTimescale1" onclick="RadioCheck(this)"/></td>
					<td><input type="radio" id="SuitableTimescale0" onclick="RadioCheck(this)"/></td>
					<td><input type="radio" id="SuitableTimescalenull" onclick="RadioCheck(this)"/></td>
				</tr>
				<tr id="multi-choice-row"> 
					<th></th>
					<th>Yes</th>
					<th>No</th>
					<th>N/A</th>
				</tr>
				<tr id="answer5">
					<td>Does the time allocated to the students to complete the task/paper appear
suitable? </td>
					<td><input type="radio" id="AppropriateLevel1" onclick="RadioCheck(this)"/></td>
					<td><input type="radio" id="AppropriateLevel0" onclick="RadioCheck(this)"/></td>
					<td><input type="radio" id="AppropriateLevelnull" onclick="RadioCheck(this)"/></td>
				</tr>
				<tr id="multi-choice-row"> 
					<th></th>
					<th>Yes</th>
					<th>No</th>
					<th>N/A</th>
				</tr>
				<tr id="answer6">
					<td>Does the marking scheme/assessment criteria appear to be appropriate? </td>
					<td><input type="radio" id="AppropriateMarkScheme1" onclick="RadioCheck(this)"/></td>
					<td><input type="radio" id="AppropriateMarkScheme0" onclick="RadioCheck(this)"/></td>
					<td><input type="radio" id="AppropriateMarkSchemenull" onclick="RadioCheck(this)"/></td>
				</tr>
				<tr id="multi-choice-row"> 
					<th></th>
					<th>Yes</th>
					<th>No</th>
					<th>N/A</th>
				</tr>
				<tr id="answer7">
					<td>Is the marking scheme consistent with the University’s Generic Grading
Descriptors?</td>
					<td><input type="radio" id="ConsistentMarkScheme1" onclick="RadioCheck(this)"/></td>
					<td><input type="radio" id="ConsistentMarkScheme0" onclick="RadioCheck(this)"/></td>
					<td><input type="radio" id="ConsistentMarkSchemenull" onclick="RadioCheck(this)"/></td>
				</tr>
				<tr id="multi-choice-row"> 
					<th></th>
					<th>Yes</th>
					<th>No</th>
					<th>N/A</th>
				</tr>
				<tr id="answer8">
					<td>Is the assessment devised in such a way to limit academic misconduct and
promote assessment for learning?</td>
					<td><input type="radio" id="AcademicMisconductLimited1" onclick="RadioCheck(this)"/></td>
					<td><input type="radio" id="AcademicMisconductLimited0" onclick="RadioCheck(this)"/></td>
					<td><input type="radio" id="AcademicMisconductLimitednull" onclick="RadioCheck(this)"/></td>
				</tr>
				<tr id="multi-choice-row"> 
					<th></th>
					<th>Yes</th>
					<th>No</th>
					<th>N/A</th>
				</tr>
				<tr id="answer9">
					<td>Does the task require second marking and has the marker been identified?</td>
					<td><input type="radio" id="SecondMarking1" onclick="RadioCheck(this)"/></td>
					<td><input type="radio" id="SecondMarking0" onclick="RadioCheck(this)"/></td>
					<td><input type="radio" id="SecondMarkingnull" onclick="RadioCheck(this)"/></td>
				</tr>
				<tr id="multi-choice-row"> 
					<th></th>
					<th>Yes</th>
					<th>No</th>
					<th>N/A</th>
				</tr>
				<tr id="answer10">
					<td>If relevant, does the assessment have ethical approval?</td>
					<td><input type="radio" id="EthicalApproval1" onclick="RadioCheck(this)"/></td>
					<td><input type="radio" id="EthicalApproval0" onclick="RadioCheck(this)"/></td>
					<td><input type="radio" id="EthicalApprovalnull" onclick="RadioCheck(this)"/></td>
				</tr>
				<tr id="multi-choice-row"> 
					<th></th>
					<th>Yes</th>
					<th>No</th>
					<th>N/A</th>
				</tr>
				<tr id="answer11">
					<td>If relevant, has a risk assessment been completed and approved? </td>
					<td><input type="radio" id="RiskAssesment1" onclick="RadioCheck(this)"/></td>
					<td><input type="radio" id="RiskAssesment0" onclick="RadioCheck(this)"/></td>
					<td><input type="radio" id="RiskAssesmentnull" onclick="RadioCheck(this)"/></td>
				</tr>
				<tr id="multi-choice-row"> 
					<th></th>
					<th>Yes</th>
					<th>No</th>
					<th>N/A</th>
				</tr>
				<tr id="answer12">
					<td>Is the reassessment method the same as the original assessment?</td>
					<td><input type="radio" id="SameReassessment1" onclick="RadioCheck(this)"/></td>
					<td><input type="radio" id="SameReassessment0" onclick="RadioCheck(this)"/></td>
					<td><input type="radio" id="SameReassessmentnull" onclick="RadioCheck(this)"/></td>
				</tr>
				<tr> 
					<th colspan="4">The internal reviewer(s) must provide details for any ‘No’ responses above and provide an overall
evaluation of the assessment:</th>
				</tr>
				<tr>
					<td colspan="4"><textarea ></textarea></td>
				</tr>
				<tr>
					<td colspan="4"><p class="error-text" id="Error"></p><button type="submit" class="med-input">Submit</button></td>
				</tr>
			</table>
		</form>
        </div>
    </body>
</html>';




?>