<?php
include '../../Database/login-check.php';
echo '<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Coursework</title>
        <link rel="stylesheet" href="style.css"/>
        <link rel="icon" type="image/png" href="src/small-logo.png">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
        <script src="script.js"></script>

        <script>
        function getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return \'\';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

        function checkParamExists(param,value){
                if(value != "" && value != null){
                    var string = param+value;
                    return string;
                } else {
                    return "";
                }
        }
        </script>
        ';

echo '<script type="text/javascript">
        $("document").ready(function(){
           $.ajax({
                type: "get",
                url: "../../Database/GetFromDatabase.php",
                data: "Field=Review"+checkParamExists("&CourseworkID=",getParameterByName("CourseworkID")),
                dataType: "json",
                success: function(response) {

                    var input_wrapper = "";
                    var toggleInt = 1;
                    var arrayInt = 0;
                    if(response!=null){
                        Object.keys(response).forEach(function(key) {

                            var Title = response[key].Title;
                            var Passed = response[key].Passed;
                            if(Passed == 1){
                            	Passed = "Passed";
                            } else if (Passed == 0){
                            	Passed = "Failed";
                            } else {
                            	Passed = "Unchecked";
                            }

                            input_wrapper += "<input id=\'toggle"+toggleInt+"\' class=\'toggle\' type=\'checkbox\' onclick=\'Toggling(id)\'/><label class=\'toggle-label\' id=\'label"+toggleInt+"\' for=\'toggle"+toggleInt+"\'><h3>"+Title+" - (ACW ID HERE)</h3></label><div class=\'collapse-wrapper\' id=\'collapse"+toggleInt+"\'><div id=\'acw-status-wrapper\'><p>"+Passed+"</p></div><input id=\'details-button\' type=\'button\' onclick=\'OpenReviewForm(getParameterByName(\"CourseworkID\") )\' value=\'Details\'/></div>";

                            toggleInt++;

                        });

                    }

                    document.getElementById("response").innerHTML = input_wrapper;
                }
            });

        });
        </script>';

echo '</head>
    <body>
        <header>
            <h1>Review</h1>
        </header>';

include 'navbar.php';
echo '<div class="content">
                <div id="acw-wrapper">';

echo '<p id="response">Loading...</p>';
echo '


             </div>
        </div>
    </body>
</html>';
