function Validation(){

	var un, pw; //Variable declarations

	un = document.forms["login"]["username"].value; //Sets un to the username value
	pw = document.forms["login"]["password"].value; //Sets pw to the password value

	if (un.trim() == null || un.trim() == "") {
		//if the e-mail is empty it's invalid.
		alert("Username is invalid");
		return false;
	}
	else if(un.includes(' ')){
		//if the e-mail contains spaces it's invalid.
		alert("Username cannot contain spaces.");
		return false;
	}
	else if (!un.includes('@')) {
		//if the e-mail doesn't contain the @ symbol then it is invalid.
		alert("Please enter a valid e-mail.")
		return false;
	}
	else if (!un.endsWith("hull.ac.uk")) {
		//if the e-mail isn't a university of hull e-mail then it is invalid.
		alert("Please use a University of Hull e-mail.");
		return false;
	}
}

function FeedbackValidation(form){
    var formChildren = form.children;
    var error = document.getElementById("Error");
    error.innerHTML = null;
    for(var i = 0; i < formChildren.length; i++){
        if(formChildren[i].value.trim() == null || formChildren[i].value.trim() == ""){
            error.innerHTML = "Please fill in every field."
            return false;
        }
    }
}

function LoggedInCheck(session){
    //This method checks to make sure the session status is either success or fail
    //if the session is a fail then use red words
    if(session == "fail"){
        document.getElementById("errTxt").innerHTML = "Your e-mail or password is incorrect.";
    }
    //else if the session isn't set do nothing yet just return
    else {
        return;
    }
}

function RadioCheck(ele){
    var value = ele.checked;
    var parentId = ele.parentElement.parentElement.getAttribute("id");
    var parent = document.getElementById(parentId);
    var radios = parent.children;
    
    for(var i = 1; i < radios.length; i++){
        var radio = radios[i].firstElementChild;
        if(radio == ele){
            continue;
        }
        else if (value && radio.value){
            radio.checked = false;
        }
    }
}

function RadioValidation(){
    var rows = document.getElementsByTagName("tr");
    var errorText = document.getElementById("Error");
    errorText.innerHTML = null;
    var errmsg = "Please complete every field.";
    var no = false;
    for(var i = 0; i < rows.length; i++){
        var input = rows[i].firstElementChild.firstElementChild;
        if(i < 6){
            if (input.value == null || input.value == "" || input.value == " ") {
                //Error message
                errorText.innerHTML = errmsg;
                return false;
            }
            else{
                continue;
            }
        }
        else if(i == 6){
            continue;
        }
        else if(i > 6 && i < rows.length - 3){
            var radios = rows[i].children;
            var anyChecked = false;
            for(var x = 1; x < radios.length; x++){
                var radio = radios[x].firstElementChild;
                if(radio.checked == true){
                    if(x == 2){
                        no = true;
                    }
                    anyChecked = true;
                    break;
                }
            }
            if(!anyChecked){
                //Error message
                errorText.innerHTML = errmsg;
                return false;
            }
            i++;
        }
        else if(i == rows.length - 2){
            if(input.value == "" && no){
                //Error message
                errorText.innerHTML = errmsg;
                return false;
            }
        }
    }
}

function Toggling(id){
    
    var toggle = document.getElementById(id);
    
    var splitId = id.split("e",2);
    
    var collapseId = "collapse" + splitId[1];
    var labelId = "label" + splitId[1];
    
    if(toggle.checked == true){
        //show
        document.getElementById(collapseId).setAttribute("style","height:auto");
    }
    else{
        //hide
        document.getElementById(collapseId).setAttribute("style","height:0px");
    }
}

//Filters the table live.
function Filter(id){    
    var input = document.getElementById(id);
    var filter = input.value.toUpperCase();
    var table = document.getElementById("person-table");
    var rows = table.getElementsByTagName("tr");
    for(var i = 0; i < rows.length; i++){
        if(id == "search"){
            var cols = rows[i].getElementsByTagName("td")[0];
            if(cols){
                if(cols.innerHTML.toUpperCase().indexOf(filter) > -1){
                    rows[i].style.display = "";
                }
                else{
                    rows[i].style.display = "none";
                }
            }
        }
        else if(id == "role"){
            if(filter == "ROLE"){
                rows[i].style.display = "";
                continue;
            }
            var cols = rows[i].getElementsByTagName("td")[2];
            if(cols){
                if(cols.innerHTML.toUpperCase() == filter){
                    rows[i].style.display = "";
                }
                else{
                    rows[i].style.display = "none";
                }
            }
        }
    }
}

function GoToCourseworkView(element){
    //Use anywhere to go to coursework.php page
    var collapseId = element.parentElement.getAttribute("id");
    var collapseElement = document.getElementById(collapseId);
    var collapseChildren = collapseElement.children;
    var tableElement = collapseChildren[0];
    var rowCount = tableElement.rows.length;
    var moduleHeader = "Module Code:";
    var moduleCode;
    
    if(rowCount == 2 && tableElement.rows[0].cells[0].innerHTML == moduleHeader){
        moduleCode = tableElement.rows[1].cells[0].innerHTML;
    }
    
    location.href = "coursework.Php?ModuleCode=" + moduleCode;
}

function GoToSpecView(id){
    if(id){
        location.href = "specification-view.php?SpecID=" + id;
    }
}

function GoToModulesView(){
    //Use anywhere to go to modules.php page
    location.href = "modules.php";
}

function GoToPeopleView(){
    //Use anywhere to go to people.php page
    location.href = "people.php";
}

function EditDeliverable(id){
    //Whatever variable is passed in is checked for possible false values and passes it through the link
    if(id){
        location.href = "deliverable-edit.php?DeliverableId=" + id;
    }
}

function NewDeliverable(courseworkId){
    //Whatever variable is passed in is checked for possible false values and passes it through the link
    if(courseworkId){
        location.href = "deliverable-add.php?courseworkId=" + courseworkId; //Assuming the deliverable page is called deliverable-add.php
    }
}

function ViewReview(courseworkId){
    //Whatever variable is passed in is checked for possible false values and passes it through the link
    if(courseworkId){
        location.href = "review.php?CourseworkID=" + courseworkId; //Assuming the deliverable page is called deliverable-add.php
    }
}

function OpenReviewForm(courseworkId){
    //Whatever variable is passed in is checked for possible false values and passes it through the link
    if(courseworkId){
        location.href = "review-form.php?CourseworkID=" + courseworkId; //Assuming the deliverable page is called deliverable-add.php
    }
}