<?php
echo'<html>
    <head>
        <meta charset="UTF-8">
        <title>Specification View</title>
        <link rel="stylesheet" href="style.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
            <script>
        function getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return \'\';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

        function checkParamExists(param,value){
                if(value != "" && value != null){
                    var string = param+value;
                    return string;
                } else {
                    return "";
                }
        }
        </script>
            <script type="text/javascript">
        $("document").ready(function(){
           $.ajax({
                type: "get",
                url: "../../Database/GetFromDatabase.php",
                data: "Field=Spec"+checkParamExists("&SpecID=",getParameterByName("SpecID")),
                dataType: "json",
                success: function(response) {
                    var thisSpec = "";
                    if(response!=null){
                        Object.keys(response).forEach(function(key) {

                            //console.log(key, obj[key]);
                            var desc = response[key].Description;
                            var title = response[key].Title;
                            thisSpec += \'<h2>\'+title+\'</h2>\';
                            thisSpec += \'<p>\'+desc+\'</p>\';
                    });

                    }
                         
                    document.getElementById("content").innerHTML = \'<div id="spec-wrapper">\'+thisSpec+\'</div>\';

                }
            });

        });
        </script>
    </head>
    <body>
        <header>
            <h1>Specification View</h1>

        </header>
        ';
        include 'navbar.php';
        echo'
        <div class="content" id="content">
        <p id="placeholder">Loading...</p>
        </div>
    </body>
</html>';

?>