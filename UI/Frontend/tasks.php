<?php
include '../../Database/login-check.php';
echo '<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Tasks</title>
        <link rel="stylesheet" href="style.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
        <script src="script.js"></script>
    </head>
    <body>
        <header>
            <h1>Tasks</h1>';

echo '<script type="text/javascript">
        $("document").ready(function(){
           $.ajax({
                type: "get",
                url: "../../Database/GetFromDatabase.php",
                data: "Field=Tasks",
                dataType: "json",
                success: function(response) {

                    var redState ="";
                    var yellowState = "";
                    var completeState = "";
                    var input_wrapper = "";
                    var toggleInt = 0;
                    var arrayInt = 0;
                    if(response!=null){
                        Object.keys(response).forEach(function(key) {
                            var State = response[key].State;
                            var Description = response[key].Description;
                            var ExpiryDate = response[key].ExpiryDate;
                            var CourseworkTitle = response[key].CourseworkTitle;
                            var Message = response[key].Message;


                            var today = new Date();
                            var ExpiryAsDate = new Date(ExpiryDate);//.toJSON().slice(0,10);
                            var timeDiff = Math.abs(ExpiryAsDate.getTime() - today.getTime());
                            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

                            var thisTask = \'<a class="unstyled-link" href="#"><div id="task-wrapper"> \';
                            var type = "";
                            switch(State){
                                case "1":
                                    if(diffDays<4){
                                        type = "high";
                                        thisTask += \' <div class="high-priority-task" id="task-priority-banner"\';
                                    } else if (diffDays < 11) {
                                        type = "medium";
                                        thisTask += \' <div class="medium-priority-task" id="task-priority-banner"\';
                                    } else {
                                        type = "complete";
                                        thisTask += \' <div class="complete-priority-task" id="task-priority-banner"\';
                                    }
                                break;

                                case "2":
                                    if(diffDays<5){
                                        type = "high";
                                        thisTask += \' <div class="high-priority-task" id="task-priority-banner"\';
                                    } else if (diffDays < 11) {
                                        type = "medium";
                                        thisTask += \' <div class="medium-priority-task" id="task-priority-banner"\';
                                    } else {
                                        type = "complete";
                                        thisTask += \' <div class="complete-priority-task" id="task-priority-banner"\';
                                    }
                                break;

                                case "3":
                                    type = "high";
                                    thisTask += \' <div class="high-priority-task" id="task-priority-banner"\';
                                break;

                                case "4":
                                    type = "high";
                                    thisTask += \' <div class="high-priority-task" id="task-priority-banner"\';
                                break;

                                case "5":
                                    type = "high";
                                    thisTask += \' <div class="high-priority-task" id="task-priority-banner"\';
                                break;

                                default:
                                    type = "complete";
                                    thisTask += \' <div class="complete-priority-task" id="task-priority-banner"\';
                                break;
                            }


                            if(CourseworkTitle != null && CourseworkTitle != "")
                            {
                                var Message = "Task: "+Description+" is due at "+new Date(ExpiryDate).toLocaleString([], {hour: \'2-digit\', minute:\'2-digit\'});
                                thisTask += \' ><div class="split-col" id="task-title"><h2 class="light-text"><b>\'+CourseworkTitle+\': \'+Description+\'</b></h2></div><div class="split-col" id="task-due-date"><h2>\'+diffDays+\' days</h2></div></div><div id="task-content"><p>\'+Message+\'</p></div></div></a>\';
                            } else if (CourseworkTitle == "" && Message != ""){
                                thisTask += \' ><div class="split-col" id="task-title"><h2 class="light-text"><b>\'+Description+\'</h2></div><div class="split-col" id="task-due-date"><h2>\'+diffDays+\' days</h2></div></div><div id="task-content"><p>\'+Message+\'</p></div></div></a>\';
                            } else {
                                thisTask += \' ><div class="split-col" id="task-title"><h2 class="light-text"><b>\'+Description+\'</h2></div><div class="split-col" id="task-due-date"><h2>\'+diffDays+\' days</h2></div></div><div id="task-content"><p>\'+Message+\'</p></div></div></a>\';
                            }

                            if(type == "high"){
                                redState += thisTask;
                            } else if(type == "medium"){
                                yellowState += thisTask;
                            } else {
                                completeState += thisTask;
                            }


                            toggleInt++;

                        });

                    }

                    document.getElementById("response").innerHTML = redState+yellowState+completeState;
                }
            });

        });
        </script>';
echo '</header>';

include 'navbar.php';

echo '
        <div class="medium-content">';
echo '      <p id="response">Loading...</p>
        </div>';

echo '</div>
    </body>
</html>';
